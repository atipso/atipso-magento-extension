<?php
/**
 * Catalog category api - overrides Mage_Catalog_Model_Category_Api
 *
 * @category   Atipso
 * @package    Atipso_Atipapi
 * @author     Sigi Eisenreich <sigi@nnpro.at>
 */
class Atipso_Atipapi_Model_Core_Api extends Mage_Catalog_Model_Category_Api
{
    
	/**
     * Retrieve info array of all existing storeviews
     *
     * @return Mage_Core_Model_Config
     */
	public function getStores(){
		$data = Mage::getModel('core/store/views')->getCollection()->toArray(); 
		$stores = $data['items'];
		$modified_stores = array();
		foreach ($stores as $store) {
			$store_locale = Mage::getStoreConfig('general/locale/code', $store['store_id']);
			$store['locale'] = $store_locale;
			array_push($modified_stores, $store);
		}
		return $modified_stores;
	}
	
	/**
     * Retrieve config value for store by path
     *
     * @param string $path for Example 'design/header/logo_src', 'general/locale/code', 
     * @param mixed $store
     * @return mixed
     */
    public function getStoreConfig($path, $store = null){
        return Mage::getStoreConfig($path, $store);
    }

	/**
	 * 
	 * Retrieve the version of Magento.
	 * @param string $asType Should be 'array' if we want the method to return array
	 * @return mixed Either string or array (if the passed param is 'array')
	 */
	public function currentVersion($asType = '')
	{
		if($asType == 'array') {
			return Mage::getVersionInfo();
		}
		
		return Mage::getVersion();
	}
	
	/**
	 * 
	 * Retrieve the version of Atipso Extension.
	 * @return mixed Either string or array (if the passed param is 'array')
	 */
	public function currentAtipsoVersion()
	{

		$data = Mage::getSingleton('core/resource')->getConnection('core_read');
		$data =	$data->fetchAll("SELECT * FROM core_resource WHERE code = 'atipapi_setup';");
		
		return $data;
	}
	
	/**
     * Get root category id
     *
     * @return int Zero if none
     */
    public function getRootCategoryId($storeId=NULL)
    {
    		try {
			$s = Mage::getModel('core/store')->load($storeId);
			
			return $s->getRootCategoryId();
		} 
		catch (Exception $e) {
			return 0;
		}
    }

	/**
     * Mage log facility
     *
     * @param string $message
     * @param integer $level
     * @param string $file
     * @param bool $forceLog
     * @return string Returns string, 'true' if OK, or exception message if exception
     */
	public function log($message, $level = null, $file = '', $forceLog = false)
    {
    	try {
    		Mage::log($message, $level, $file, $forceLog);
    		return 'true';	
    	}
    	catch (Exception $e) {
    		return $e->getMessage();
    	}
		
    }

}