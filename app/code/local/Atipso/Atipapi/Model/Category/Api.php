<?php
/**
 * Catalog category api - overrides Mage_Catalog_Model_Category_Api
 *
 * @category   Atipso
 * @package    Atipso_Atipapi
 * @author     Peter Arvenom <peter.arvenom@gmail.com>
 */
class Atipso_Atipapi_Model_Category_Api extends Mage_Catalog_Model_Category_Api
{
    protected $_filtersMap = array(
        'product_id' => 'entity_id',
        'set'        => 'attribute_set_id',
        'type'       => 'type_id'
    );

    /**
     * Retrieve list of assigned products to category
     *
     * @param int $categoryId
     * @param string|int $store
     * @param Array $filters
     * @return array
     */
    public function assignedProductsDetail($categoryId, $store = null, $filters = null)
    {
		// $visibility = array(  
        //    Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,  
        //    Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG  
        // );

		// // Filter by products types if specified 
		$excludeProductTypes = array();
		if (isset($filters['exclude_product_types']) && !empty($filters['exclude_product_types'])) {
			$excludeProductTypes = $filters['exclude_product_types'];
		}

		$category = $this->_initCategory($categoryId);
		
        $collection = Mage::getModel('catalog/product')->getCollection()
			// ->addStoreFilter($store)
			// ->addStoreFilter($this->_getStoreId($store))
            ->setStoreId($this->_getStoreId($store))
            ->addCategoryFilter($category);
		
		if (!empty($excludeProductTypes)) {
			$collection->addAttributeToFilter('type_id', array( 'nin' => $excludeProductTypes ));
		}
		
		$collection->addAttributeToFilter('visibility', array('neq' => 1))
			// ->addAttributeToFilter('type_id', array( 'nin' => array('grouped', 'configurable') ))
            ->addAttributeToSelect('*')
			// ->addAttributeToSelect('minimal_price')
			// ->addAttributeToSelect('price')
			// ->addMinimalPrice()
			// ->addFinalPrice()
			->addTaxPercents()
			// // Next line prevents items from showing up with price '0' in the list
            ->applyFrontendPriceLimitations();
			
			
			
			
			
		// // Before we add paging values, clone the 'collection' 
		// // which will be used in getting the record count.
        $kollection = Mage::getModel('catalog/product')->getCollection()
			// ->addStoreFilter($store)
			// ->addStoreFilter($this->_getStoreId($store))
            ->setStoreId($this->_getStoreId($store))
            ->addCategoryFilter($category);
		
		if (!empty($excludeProductTypes)) {
			$kollection->addAttributeToFilter('type_id', array( 'nin' => $excludeProductTypes ));
		}
		
		$kollection->addAttributeToFilter('visibility', array('neq' => 1))
			// ->addAttributeToFilter('type_id', array( 'nin' => array('grouped', 'configurable') ))
            ->addAttributeToSelect('entity_id');

			

		$excludeFromFilterLogic = array('page_size', 'page_number', 'exclude_product_types');
        if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
					if (in_array($field, $excludeFromFilterLogic)) {
						continue;
					}

                    if (isset($this->_filtersMap[$field])) {
                        $field = $this->_filtersMap[$field];
                    }
					
                    $collection->addFieldToFilter($field, $value);
					$kollection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }
		

        // // The next code block sets up paging parameters
		$pageNumber = -1;
		$pageSize = -1;
		if (is_array($filters)) {
			if (isset($filters['page_number'])) {
				$pageNumber = $filters['page_number'];
			}
			
			if (isset($filters['page_size'])) {
				$pageSize = $filters['page_size'];
			}
		}
		if ($pageNumber != -1) {
			$collection->setPage($pageNumber);
		}
		if ($pageSize != -1) {
			$collection->setPageSize($pageSize);
		}

        // // The next line ensures pagination takes place
		if ($pageNumber != -1 || $pageSize != -1) {
			$collection->load();
		}
		
		// // Since our result expects:
		// // 'product_id' => 'entity_id'
		// // amongst others, we'll do that remap here
		$flippedFiltersMap = array_flip($this->_filtersMap);
		$result = array();
        foreach ($collection as $product) {
            // $result[] = $product->toArray();
			
			// error_log( 'getImageUrl : [' . $product->getImageUrl() . ']');
			// error_log( 'getSmallImageUrl : [' . $product->getSmallImageUrl() . ']');
			// error_log( 'getThumbnailUrl : [' . $product->getThumbnailUrl() . ']');
			// error_log( 'getTaxPercent : [' . $product->getTaxPercent() . ']');

			// // Switch back to our preferred product property names
			$arrayedProduct = $product->toArray();
			foreach ($flippedFiltersMap as $field => $value) {
				$arrayedProduct[$value] = $arrayedProduct[$field];
				unset($arrayedProduct[$field]);
			}
            $result[] = $arrayedProduct;
        }
		
		// // Just to be safe, load the kollection up to 
		// // ascertain that we get the correct record count.
		$kollection->load();
		$recordCount = count($kollection);
		
		// // Get incendiary info here
		$currency_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore($store)->getCurrentCurrencyCode())->getSymbol();
		$price_includes_tax = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX, $store);

        // return $result;
        return array(
			'products' => $result, 
			'record_count' => $recordCount, 
			'currency_symbol' => $currency_symbol,
			'price_includes_tax' => $price_includes_tax);
    }

    /**
     * Retrieves:
	 * 1) id & name of main categories
	 * 2) id & name of 5 products in each category in (1)
	 * 3) image url for a random item within (2) above
     *
     * @param string|int $store
     * @param Array $filters
     * @return array
     */
    public function randomProductsForCategories($store = null, $filters = null)
    {
		/*
		// // ATTEMPT 1
		// // Returns a flatened list of the category tree
		$category = Mage::getModel('catalog/category');
		$tree = $category->getTreeModel();
		$tree->load();

		$ids = $tree->getCollection()->getAllIds();
		$result = array();

		if ($ids){
			foreach ($ids as $id){
				$cat = Mage::getModel('catalog/category');
				$cat->load($id);
				// array_push($result, $cat);
				array_push($result, array(
					'id' => $cat['entity_id'], 
					'name' => $cat['name'], 
					'parent_id' => $cat['parent_id']
				));
			}
		}
		*/

		/* 
		// // ATTEMPT 2
		// // Returns empty. Why ? Whatever ...
		$helper = Mage::helper('catalog/category');
		// sorted by name, fetched as collection
		$categories = $helper->getStoreCategories('name', true, false);
		*/

		/*
		// // ATTEMPT 3
		// // This will only work if the store has 'featured' attribute defined. 
		// // Otherwise, there'll be a nasty error.
		$layer = Mage::getSingleton('catalog/layer');
		$collection = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToFilter('featured', 1);        
		$layer->prepareProductCollection($collection);    
		$result =  $collection;
		*/
		
		// // ATTEMPT 4
		// // Dumnp list of stores this server has
		// $stores = Mage::app()->getStores(false, true);
		// foreach ($stores as $store) {
		//	$storeDump = '[' . $store->getCode() . '];[' . $store->getId() .']';
		//	error_log($storeDump);
		// }

		// // Fetches products from the immediate sub categories of the main categories.
		// // Why ? Normally main categories do not have products.
		$categories = Mage::getModel('catalog/category')->getCollection()
			// ->addStoreFilter($store)
			// ->addStoreFilter($this->_getStoreId($store))
			->setStoreId($this->_getStoreId($store))
			// ->addAttributeToSelect('url_path')
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('name')
			->addAttributeToSelect('parent_id')
			->addAttributeToSelect('level');

		// // Filter by category if ids were specified
		if (isset($filters['category_ids']) && !empty($filters['category_ids'])) {
			// error_log('category_ids => ' . print_r($filters['category_ids'], true));
			$categories->addAttributeToFilter('entity_id', array('in' => $filters['category_ids']));
		}

		$categories->addAttributeToFilter('level', array('eq' => 2))
			// ->addAttributeToFilter('is_active', array('eq' => 1))
			->addIsActiveFilter()
			// // What if the root is not 3 ?
			// ->addAttributeToFilter('parent_id', array('eq' => 3))
			// // This includes empty & root categories too. Weird ...
			// ->addLevelFilter(2)
			->addOrderField('name');

		$categories->load();
		// error_log('Got ' . count($categories) . ' categories');

		// // This first iteration gets the root category. 
		// // The id is normally 3 in most stores but lets not take that chance :)
		// // Note that the default category names isn't so easily
		// // altered from the Magento backend by store owners.
		// $rootId = -1;
		// $needle = 'efault Category';
		// foreach ($categories as $category) {
			// $haystack = $category['name'];
			// $pos = strpos($haystack, $needle);
			// if ($pos !== false) {
				// $rootId = $category['entity_id'];
				// break;
			// }
		// }
		// if ($rootId == -1) {
			// $needle = 'oot Catalog';
			// foreach ($categories as $category) {
				// $haystack = $category['name'];
				// $pos = strpos($haystack, $needle);
				// if ($pos !== false) {
					// $rootId = $category['entity_id'];
					// break;
				// }
			// }
		// }
		

		$catalogImageHelper = Mage::helper('catalog/image');

		// // Filter by products types if specified 
		$excludeProductTypes = array();
		if (isset($filters['exclude_product_types']) && !empty($filters['exclude_product_types'])) {
			$excludeProductTypes = $filters['exclude_product_types'];
		}

		$imageWidth = 100;
		if (isset($filters['image_width']) && !empty($filters['image_width'])) {
			$imageWidth = $filters['image_width'];
		}

		$imageHeight = 100;
		if (isset($filters['image_height']) && !empty($filters['image_height'])) {
			$imageHeight = $filters['image_height'];
		}

		$result = array();

		foreach ($categories as $category) {
			// // If its not the main category, skip
			// if ($category['parent_id'] != $rootId) { continue; }
			// if ($category['level'] != 2) { continue; }
						
			// // Get random products from the category itself
			$products = Mage::getModel('catalog/product')->getCollection()
				// ->addStoreFilter($store)
				->addStoreFilter($this->_getStoreId($store))
				->addAttributeToSelect('id')
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				->addCategoryFilter($category);
				
			if (!empty($excludeProductTypes)) {
				$products->addAttributeToFilter('type_id', array( 'nin' => $excludeProductTypes));
			}
			
			$products->addAttributeToFilter('visibility', array('neq' => 1))
				// ->addAttributeToFilter('type_id', array( 'nin' => array('grouped', 'configurable') ))
				->addAttributeToSort('news_from_date', 'desc')
				->setPageSize(5);
				
			// error_log('Got ' . count($products) . ' products for category ' . $category['name']);

			// // If there are results, then use them.
			// // If not get products from immediate child categories.
			if (count($products) > 0) {

				$resultProducts = array();
				foreach ($products as $product) {
					array_push($resultProducts, array(
						'id' => $product->getId(),
						'name' => $product->getName(),
						// 'image' => $product->getImageUrl()
						'image' => (string)$catalogImageHelper->init($product, 'image')->resize($imageWidth, $imageHeight)
					));
				}
				
				array_push($result, array(
					'id' => $category['entity_id'], 
					'name' => $category['name'],
					'products' => $resultProducts
				));
			
			} else {

				// // Array consisting of all child categories id
				$allSubCategoryIds = explode(',', $category->getChildren());

				// // Filter by category if ids were specified
				$subCategoryIds = array();
				if (isset($filters['category_ids']) && !empty($filters['category_ids'])) {
					$subCategoryIds = array_intersect($allSubCategoryIds, $filters['category_ids']);
				} else {
					$subCategoryIds = $allSubCategoryIds;
				}

				// error_log('subCategoryIds = ' . print_r($subCategoryIds, true));
				
				foreach ($subCategoryIds as $subCategoryId) {
					$subCategory = Mage::getModel('catalog/category')->load($subCategoryId);
					
					// // Get 5 random products from it
					$products =  Mage::getModel('catalog/product')->getCollection()
						// ->addStoreFilter($store)
						->addStoreFilter($this->_getStoreId($store))
						->addAttributeToSelect('id')
						->addAttributeToSelect('name')
						->addAttributeToSelect('image')
						// ->addCategoryFilter($subCategoryId)
						->addCategoryFilter($subCategory);
						
					if (!empty($excludeProductTypes)) {
						$products->addAttributeToFilter('type_id', array( 'nin' => $excludeProductTypes));
					}
					
					$products->addAttributeToFilter('visibility', array('neq' => 1))
						// ->addAttributeToFilter('type_id', array( 'nin' => array('grouped', 'configurable') ))
						->addAttributeToSort('news_from_date', 'desc')
						->setPageSize(5);
						
					// error_log('Got ' . count($products) . ' products for sub category ' . $subCategory['name']);
					
					// // Only add to result if there's any products
					if (count($products) == 0) { continue; }

					$resultProducts = array();
					foreach ($products as $product) {
						array_push($resultProducts, array(
							'id' => $product->getId(),
							'name' => $product->getName(),
							// 'image' => $product->getImageUrl()
							'image' => (string)$catalogImageHelper->init($product, 'image')->resize($imageWidth, $imageHeight)
						));
					}
					
					array_push($result, array(
						'id' => $subCategory['entity_id'], 
						'name' => $subCategory['name'],
						'products' => $resultProducts
					));
				} // // end : foreach ($subCategoryIds as $subCategoryId)
				
			} // // end : if (count($products) > 0)
			
 		} // // end : foreach ($categories as $category)

		/*
		// // ATTEMPT 5
		// // Returns products from the flatened list of the category tree
		$tree = Mage::getModel('catalog/category')->getTreeModel();
		$tree->load();
		$ids = $tree->getCollection()->getAllIds();

		$result = array();

		foreach ($ids as $id) {
			$category = Mage::getModel('catalog/category')->load($id);
			
			// // Get 5 random products from it
			$products =  Mage::getModel('catalog/product')->getCollection()
				->addAttributeToSelect('id')
				->addAttributeToSelect('name')
				->addAttributeToSelect('image')
				// ->addCategoryFilter(Mage::getModel('catalog/category')->load($id))
				->addCategoryFilter($category)
				->addAttributeToFilter('visibility', array('neq' => 1))
				->addAttributeToSort('news_from_date', 'desc')
				->setPageSize(5);
				
			// $products->load();
			
			// // only add to result if there's any products
			if (count($products) == 0) { continue; }

			$resultProducts = array();
			foreach ($products as $product) {
				array_push($resultProducts, array(
					'id' => $product->getId(),
					'name' => $product->getName(),
					'image' => $product->getImageUrl()
				));
			}
			
			array_push($result, array(
				'id' => $category['entity_id'], 
				// 'parent_id' => $category['parent_id'],
				'name' => $category['name'],
				'products' => $resultProducts
			));
		}
		*/

		
		// // Get incendiary info here
		$currency_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore($store)->getCurrentCurrencyCode())->getSymbol();

        // return $result;
        return array(
			'categories' => $result, 
			'currency_symbol' => $currency_symbol);
	}
	
	
	/**
     * Retrieve category tree
     *
     * @param int $parent
     * @param string|int $store
     * @return array
     */
    public function activeCategories($parentId = null, $store = null, $filters = null)
    {
        if (is_null($parentId) && !is_null($store)) {
            $parentId = Mage::app()->getStore($this->_getStoreId($store))->getRootCategoryId();
        } elseif (is_null($parentId)) {
            $parentId = 1;
        }

        /* @var $tree Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Tree */
        $tree = Mage::getResourceSingleton('catalog/category_tree')
            ->load();

        $root = $tree->getNodeById($parentId);

        if($root && $root->getId() == 1) {
            $root->setName(Mage::helper('catalog')->__('Root'));
        }

        $collection = Mage::getModel('catalog/category')->getCollection()
			// ->addStoreFilter($store)
			// ->addStoreFilter($this->_getStoreId($store))
			->setStoreId($this->_getStoreId($store))
			->addIsActiveFilter()
			->addFieldToFilter('include_in_menu', 1)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('is_active')
			->addAttributeToSelect('children_count')
			->addAttributeToSelect('include_in_menu')
			->addAttributeToSelect('is_anchor');

        $tree->addCollectionData($collection, true);

		// // Filter by products types if specified 
		$excludeProductTypes = array();
		if (isset($filters['exclude_product_types']) && !empty($filters['exclude_product_types'])) {
			$excludeProductTypes = $filters['exclude_product_types'];
		}

        return $this->_nodeToArray($root, $store, $excludeProductTypes);
		//return $collection->getSize();
		
    }

	/**
     * Convert node to array
     *
     * @param Varien_Data_Tree_Node $node
     * @return array
     */
    protected function _nodeToArray(Varien_Data_Tree_Node $node, $store, $excludeProductTypes)
    {
        // Only basic category data
        $result = array();
        $result['category_id'] = $node->getId();
        $result['parent_id']   = $node->getParentId();
        $result['name']        = $node->getName();
        $result['is_active']   = $node->getIsActive();
		$result['include_in_menu']   = $node->getIncludeInMenu();
        $result['position']    = $node->getPosition();
        $result['level']       = $node->getLevel();
		$result['is_anchor']       = $node->getIsAnchor();
		$result['products_count'] = $this->countProducts($node->getId(), $store, $excludeProductTypes);
		$result['children_products_count'] = $result['products_count'];
        $result['children']    = array();

        foreach ($node->getChildren() as $child) {
            $result['children'][] = $this->_nodeToArray($child, $store, $excludeProductTypes);
			
			$sum = 0;
			foreach ($result['children'] as $sub) {
				$sum += $sub['products_count'];
			}
			$result['children_products_count'] = $sum;
        }

        return $result;
    }
	
	/**
     * Retrieve number of assigned products to category
     *
     * @param int $categoryId
     * @param string|int $store
     * @return string
     */
	protected function countProducts($categoryId, $store = null, $excludeProductTypes)
    {
		// // Remarked as it has caused so many issues where hidden products are 
		// // still considered in the product count even after we tell it not to.
		
		$category = $this->_initCategory($categoryId);
        $storeId = $this->_getStoreId($store);
        $collection = $category
			->setStoreId($storeId)
			->getProductCollection()
			->addAttributeToFilter('visibility', array('neq' => 1));
		
		if (!empty($excludeProductTypes)) {
			// $collection->addAttributeToFilter('type_id', array( 'nin' => array('grouped', 'configurable') ));
			$collection->addAttributeToFilter('type_id', array( 'nin' => $excludeProductTypes ));
		}
		
        ($storeId == 0)? $collection->addOrder('position', 'asc') : $collection->setOrder('position', 'asc');

        return $collection->getSize();
    }
}