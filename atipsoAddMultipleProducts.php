<?php
require_once ('app/Mage.php');
Mage::app();
$session = Mage::getModel('core/session', array('name' => 'frontend'));
 
/*
// // Simple products test. Works :) 
$productIds = array(51, 52);
 
$cart = Mage::getModel('checkout/cart');
$cart->addProductsByIds($productIds);
$cart->save();
*/

// // TODO : Now figure out products with configurable attributes, custom options and bundle options. Ok ...

// // Product id & options
/* 
// // These were tested OK

// //  TEST 1. Simple products
$productIds = array(51, 52);

// // TEST 2. Configurable product
$productIds = array(119);
$params = array(
	'qty' => 1,
	'super_attribute' => array(
		525 => 100
	)
);

// // TEST 3. (Simple ?) products with custom options
$productIds = array(25);
$params = array(
	'qty' => 1,
	'options' => array(
		1 => 3
	)
);

// // TEST 4. Bundle product. Magento will report these products as NOT configurable.
$productIds = array(164);
$params = array(
	'product' => 164,
	// 'related_product' => null,
	'bundle_option' => array(
		21 => 58, // NZXT Lexa Silver Aluminum ATX Mid-Tower Case (Default)
		20 => 55, // Intel C2D E8400 3.0GHz Retail
		11 => 29, // Crucial 2GB PC4200 DDR2 533MHz Memory
		12 => array(
			0 => 30 // Western Digital - 1TB HD - 7200RPM
		),
		13 => array(
			0 => 34, // Microsoft Wireless Optical Mouse 5000
			1 => 35 // Microsoft Natural Ergonomic Keyboard 4000
		)
	),
	// 'options' => array(),
	'qty' => 1
);
*/

// // TEST 5. This is a variation of TEST 4.
// // Here we get 'params' from a JSON string which is the 
// // same string from the Superstorefront widget's cookie.
$json_string = '{"items":[{"product":"119","name":"Coalesce: Functioning On Impatience T-Shirt","price":"15.00","cart_url":"http://pmg.webhop.org/magento/checkout/cart/add?product=119&qty=1","qty":"1","options":{},"super_attribute":{"525":"100"},"bundle_option":{}},{"product":"164","name":"Gaming Computer","price":"4999.95","cart_url":"http://pmg.webhop.org/magento/checkout/cart/add?product=164&qty=1","qty":"1","options":{},"super_attribute":{},"bundle_option":{"21":["58"],"20":["56"],"13":["32","33"],"11":"29","12":["31"]}},{"product":"25","name":"Apple MacBook Pro MA464LL/A 15.4\" Notebook PC","price":"2299.99","cart_url":"http://pmg.webhop.org/magento/checkout/cart/add?product=25&qty=1","qty":"1","options":{"1":"2"},"super_attribute":{},"bundle_option":{}},{"product":"51","name":"Ottoman","price":"299.99","cart_url":"http://pmg.webhop.org/magento/checkout/cart/add?product=51&qty=1","qty":"1","options":{},"super_attribute":{},"bundle_option":{}}]}';
if (isset($_POST['cart_string'])) {
	$json_string = $_POST['cart_string'];
}
error_log('json_string =  ' . $json_string);
$qartObject = json_decode($json_string);
$items = $qartObject->items;
// $item0 = $items[0];
// // The next line KO
// $params = $item0;

$cart = Mage::getModel('checkout/cart');
try {
// foreach( $productIds as $productId) {
	foreach( $items as $item) {
		$productId = $item->product;
		
		// $qty = $this->getRequest()->getParam('qty' . $productId, 0);
		// $qty = 1;
		$qty = $item->qty;

		if ($qty <= 0) continue; // nothing to add
		
		// $cart = $this->_getCart();
		$product = Mage::getModel('catalog/product')
			->setStoreId(Mage::app()->getStore()->getId())
			->load($productId);
			// ->setConfiguredAttributes($this->getRequest()->getParam('super_attribute'));
			// ->setGroupedProducts($this->getRequest()->getParam('super_group', array())); // might need this later ...
			
		/*
		$eventArgs = array(
			'product' => $product,
			'qty' => $qty,
			// 'request' => $this->getRequest(),
			// 'request' => Mage::app()->getRequest(),
			'request' => new Mage_Core_Controller_Request_Http(),
			// 'request' => $this->getResponse()
			// 'response' => Mage::app()->getResponse()
			'response' => new Mage_Core_Controller_Response_Http(),
			'additional_ids' => array()
		);

		Mage::dispatchEvent('checkout_cart_before_add', $eventArgs);

		$cart->addProduct($product, $qty);
		*/
		
		// // Convert 'option' objects to arrays as Magento accepts the latter.
		// // The irregular variable naming convention below is deliberate 
		// // in order to be in sync with the one used by Magento.
		$selectedMap = array(
			'bundle_option' => array(),
			'super_attribute' => array(),
			'options' => array() 
			
		);
		foreach ($selectedMap as $kkey => $vvalue) {
			if ( is_array($item->{$kkey}) ) {
				$vvalue = $item->{$kkey};
			} else {
				foreach($item->{$kkey} as $key => $value) {
					$vvalue[$key] = $value;
				}
			}
			// // Remember to overwrite the value for this key.
			$selectedMap[$kkey] = $vvalue;
		}
		
		$params = array(
			// 'product' => $item->product,
			'product' => $productId,
			'bundle_option' => $selectedMap['bundle_option'],
			'super_attribute' => $selectedMap['super_attribute'],
			'options' => $selectedMap['options'],
			// 'qty' => $item->qty
			'qty' => $qty
		);
		
		$cart->addProduct($product, $params);

		// Mage::dispatchEvent('checkout_cart_after_add', $eventArgs);
		
		$productNames[] = $product->getName();
	} // // end : foreach( $items as $item

	$cart->save();

	// Mage::dispatchEvent('checkout_cart_add_product', array('product'=>$product));

	// $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());    
	$message = sprintf('%s was successfully added to your shopping cart.', implode($productNames, ', '));  
	
	Mage::getSingleton('checkout/session')->addSuccess($message);
}
catch (Mage_Core_Exception $e) {
	error_log('Mage_Core_Exception : ' . $e->getMessage());
	if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
		Mage::getSingleton('checkout/session')->addNotice($product->getName() . ': ' . $e->getMessage());
	}
	else {
		Mage::getSingleton('checkout/session')->addError($product->getName() . ': ' . $e->getMessage());
	}
}
catch (Exception $e) {
	error_log('Exception : ' . $e->getMessage());
	// Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
	Mage::getSingleton('checkout/session')->addException($e, 'Can not add item to shopping cart');
}

// change to relevant URL for your store!
// header("location: http://pmg.webhop.org/magento/checkout/cart/");
header("location: checkout/cart/");