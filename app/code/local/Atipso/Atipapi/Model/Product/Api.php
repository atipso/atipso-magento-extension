<?php
/**
 * Catalog product api - overrides Mage_Catalog_Model_Product_Api
 *
 * @category   Atipso
 * @package    Atipso_Atipapi
 * @author     Peter Arvenom <peter.arvenom@gmail.com>
 */
class Atipso_Atipapi_Model_Product_Api extends Mage_Catalog_Model_Product_Api
{

    /**
     * Retrieve product info with options
     *
     * @param int|string $productId
     * @param string|int $store
     * @param array $attributes
     * @return array
     */
    public function infoEnhanced($productId, $store = null, $attributes = null, $identifierType = null)
    {
		$product = $this->_getProduct($productId, $store, $identifierType);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }
		
		// // Set tax percent (its always percent right ? right ?)
		// // Inspired by Magento's app\code\core\Mage\Tax\Model\Observer.php.
		// // Please note that this value must be manually added into the 'result'
		// // array as Magento considers it as a non-allowed attribute.
		if (null === $product->getTaxClassId()) {
			$product->setTaxClassId($product->getMinimalTaxClassId());
		}
		if (!isset($classToRate[$product->getTaxClassId()])) {
            $request = Mage::getSingleton('tax/calculation')->getRateRequest();
			$request->setProductClassId($product->getTaxClassId());
			$classToRate[$product->getTaxClassId()] = Mage::getSingleton('tax/calculation')->getRate($request);
		}
		$product->setTaxPercent($classToRate[$product->getTaxClassId()]);
		
		// // START : tests
		// // The following applies to configurable products such as  
		// // Coalesce: Functioning On Impatience T-Shirt (Shirts, page 1)
		
		// // Get configurable type product instance and get configurable attributes collection.
		// // Sample uri to add a configurable product via query string:
		// // http://www.your_domain.com/checkout/cart/add?product=68&qty=1&super_attribute[528]=55&super_attribute[525]=56
		$configurable_attributes = array();
		$used_products = array();
		if (method_exists($product->getTypeInstance(), 'getConfigurableAttributes') == true) {
			$configurableAttributeCollection=$product->getTypeInstance()->getConfigurableAttributes();
			// // Use the collection to get the designated values of each configurable attribute
			$index = 0;
			foreach($configurableAttributeCollection as $attribute){				
				// error_log( 'attribute['. $index . '] print_r:['.print_r($attribute->getProductAttribute(), true).']' );

				$productAttribute = $attribute->getProductAttribute();

				$att = array(
					'id' => $productAttribute->getId(),
					'code' => $productAttribute->getAttributeCode(),
					// // Magento sets this to 1 even for non required attributes. See the store detail view for 
					// // 'Zolof The Rock And Roll Destroyer: LOL Cat T-shirt' from the Magento sample database.
					'required' => 1, 
					// 'required' => $attribute->getIsRequired(), // KO. Returns empty
					// 'required' => $productAttribute->getIsRequire(), // KO. Returns empty
					// 'required' => $productAttribute->getRequired(), // KO. Returns empty
					// 'required' => $productAttribute->getData('required'), // KO. Returns empty
					// 'required' => $productAttribute->getData('is_required'), // Does not match those at the store 100%
					'get_is_required' => $productAttribute->getIsRequired(), // Does not match those at the store 100%
					'label' => $productAttribute->getFrontend()->getLabel(),
					// // Next one from function 'getConfigurableAttributesAsArray' in app\code\core\Mage\Catalog\Model\Product\Type\Configurable.php
					'options' => $attribute->getPrices()
					// 'options' => $productAttribute->getSource()->getAllOptions() // OK but does not match store
					// 'options' => $productAttribute->getSource()->getSelectableOptions() // KO
					// 'options' => $productAttribute->getSource()->getOptions() // KO
					// 'options' => $productAttribute->getSource()->getVisibleOptions() // KO
				);
				array_push($configurable_attributes, $att);

				$index++;
			}

			// // START : used products ?
			// // The next one returns the id of the used products
			$usedProductIds = $product->getTypeInstance(true)->getUsedProductIds($product);

			$usedProductCollection = $product->getCollection()
				// ->addAttributeToSelect('name')
				->addAttributeToSelect('sku')
				// ->addAttributeToSelect('attribute_set_id')
				// ->addAttributeToSelect('type_id')
				// ->addAttributeToSelect('price')
				->addFieldToFilter('attribute_set_id',$product->getAttributeSetId())
				->addFieldToFilter('entity_id', array('in' => $usedProductIds ))
				// ->addFieldToFilter('type_id', $allowProductTypes)
				->addFilterByRequiredOptions();

			// // The next line includes inventory count
			// Mage::getModel('cataloginventory/stock_item')->addCatalogInventoryToProductCollection($associatedProductCollection);

			// // The next block includes the super attribute i.e. size & color
			foreach ($product->getTypeInstance(true)->getUsedProductAttributes($product) as $attribute) {
				$usedProductCollection->addAttributeToSelect($attribute->getAttributeCode());
				$usedProductCollection->addAttributeToFilter($attribute->getAttributeCode(), array('nin'=>array(null)));
			}

			$used_products = array();
			foreach ($usedProductCollection as $rp) {
				$rpraw = $rp->getData();
				
				// // Remove fields we don't want which we're unable to filter when fetching the collection.
				unset($rpraw['stock_item']);
				unset($rpraw['created_at']);
				unset($rpraw['updated_at']);
				unset($rpraw['has_options']);
				unset($rpraw['required_options']);
				unset($rpraw['entity_type_id']);
				unset($rpraw['attribute_set_id']);
				// unset($rpraw['type_id']);
				
				array_push($used_products, 
					// $rp->getData()
					$rpraw
				);
			}
			// // END : used products ?
		}
		
		// // The following applies to simple products with
		// // custom options such as (page 8 under 'Computers'):
		// // Apple MacBook Pro MA464LL/A 15.4" Notebook PC (i.e. product_id 25)
		$custom_options = array();
		// if (method_exists($product, 'getAllOptions') == true) {
			// $options = $product->getAllOptions();
		if (method_exists($product, 'getOptions') == true) {
			$options = $product->getOptions();
			
			if (count($options) > 0) {
				error_log( 'info : [' . $product->getName() . '] of type [' . $product->getTypeId() . '] has [' . count($options) . '] options.' );
			}
			
			$index = 0;
			foreach ($options as $o) {
				// error_log('o = [' . print_r($o, true) . ']');
				$ov = array();
				$values = $o->getValues();
				foreach ($values as $v) {
					// // The next line has 'option_id', 'title', 'default_price' etc
					array_push($ov, $v->getData());
				}
				
				$att = array(
					'id' => $o->getId(),
					'type' => $o->getType(),
					'label' => $o->getTitle(),
					// 'required' => $o->getRequired(),
					// 'required' => $o->getData('required'),
					// 'required' => $o->getData('is_required'),
					// 'required' => $o->getIsRequired(),
					'required' => $o->getIsRequire(),
					// 'options' => array()
					'options' => $ov
				);
				array_push($custom_options, $att);

				$index++;
			}
		}
		
		// // The following applies to bundle products
		// // Sony VAIO 11.1" Notebook PC (psge 1 under 'Computers')
		// // Never realized how complicated it is to get bundle options until now.
		if (method_exists($product->getTypeInstance(true), 'getSelectionsCollection') == true) {
			// // From app\code\core\Mage\Bundle\Model\Observer.php
			$product->getTypeInstance(true)->setStoreFilter($product->getStoreId(), $product);
			$optionCollection = $product->getTypeInstance(true)->getOptionsCollection($product);
			$selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection(
				$product->getTypeInstance(true)->getOptionsIds($product),
				$product
			);
			$optionCollection->appendSelections($selectionCollection);

			$bundle_options = array();
			// $selectionRawData = array();

			$i = 0;
			foreach ($optionCollection as $option) {
				// error_log('option = [' . print_r($option, true) . ']');
				$tmpSelectionData = array();
				foreach ($option->getSelections() as $selection) {
					// error_log('selection = [' . print_r($selection, true) . ']');
					// $selectionRawData[$i][] = array(
					$tmpSelectionData[] = array(
						// 'option_id' => $selection->getOptionId(),
						// 'option_id' => $selection->getOption()->getId(),
						// 'option_label'  => $selection->getOption()->getTitle(),
						'name' => $selection->getName(),
						'selection_id' => $selection->getSelectionId(),
						'id' => $selection->getId(),
						'product_id' => $selection->getProductId(),
						// 'price' => $selection->getSelectionPrice(), // KO
						// 'price' => $selection->getData('price'), // OK
						'price' => $selection->getPrice(),
						'position' => $selection->getPosition(),
						'is_default' => $selection->getIsDefault(),
						'selection_price_type' => $selection->getSelectionPriceType(),
						'selection_price_value' => $selection->getSelectionPriceValue(),
						'selection_qty' => $selection->getSelectionQty(),
						'selection_can_change_qty' => $selection->getSelectionCanChangeQty(),
						'delete' => ''
					);
				}

				$bundle_options[$i] = array(
					'id' => $option->getData('option_id'),
					'required' => $option->getData('required'),
					'position' => $option->getData('position'),
					'type' => $option->getData('type'),
					'title' => $option->getData('title')?$option->getData('title'):$option->getData('default_title'),
					'delete' => '',
					'options' => $tmpSelectionData
				);
				
				$i++;
			}

			// $newProduct->setBundleOptionsData($bundle_options);
			// $newProduct->setBundleSelectionsData($selectionRawData);
		}
		// // END : tests
		
		$result = array( // Basic product data
            'product_id' => $product->getId(),
            'sku' => $product->getSku(),
            'set' => $product->getAttributeSetId(),
            'type' => $product->getTypeId(),
            'categories' => $product->getCategoryIds(),
            // 'websites' => $product->getWebsiteIds(),
			// // The next one is required for display but strangely is skipped by the foreach loop below.
			'tax_percent' => $product->getTaxPercent(),
			'price_includes_tax' => Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX, $store),
			'product_collection' => $filteredProducts,
            'currency_symbol' => Mage::app()->getLocale()->currency(Mage::app()->getStore($store)->getCurrentCurrencyCode())->getSymbol()
        );

		foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute, $attributes)) {
                $result[$attribute->getAttributeCode()] = $product->getData($attribute->getAttributeCode());
            }
        }		
		
		// // START : tests
		// // Add configurable attributes here.
		if (!empty($configurable_attributes)) {
			$result['configurable_attributes'] = $configurable_attributes;
			$result['used_products'] = $used_products;
		}

		// // Add custom options here.
		if (!empty($custom_options)) {
			$result['custom_options'] = $custom_options;
		}

		// // Add bundle options here.
		if (!empty($bundle_options)) {
			$result['bundle_options'] = $bundle_options;
		}
		// // END : tests

        return $result;
    }

} // Class Atipso_Catalog_Model_Product_Api End
