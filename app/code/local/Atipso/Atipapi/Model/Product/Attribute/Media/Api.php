<?php
/**
 * Catalog model product attribute media api - overrides Mage_Catalog_Model_Product_Attribute_Media_Api
 *
 * @category   Atipso
 * @package    Atipso_Atipapi
 * @author     Peter Arvenom <peter.arvenom@gmail.com>
 */
class Atipso_Atipapi_Model_Product_Attribute_Media_Api extends Mage_Catalog_Model_Product_Attribute_Media_Api
{
 
	/**
     * Retrieve images for product
     *
     * @param int|string $productId
     * @param string|int $store
     * @param any (?) $identifierType
     * @param Array $filters
     * @return array
     */
    public function itemsWithResized($productId, $store = null, $identifierType = null, $filters = null)
    {
        // error_log('itemsWithResized');
		
		$product = $this->_initProduct($productId, $store, $identifierType);

        $gallery = $this->_getGalleryAttribute($product);

        $galleryData = $product->getData(self::ATTRIBUTE_CODE);

        if (!isset($galleryData['images']) || !is_array($galleryData['images'])) {
            return array();
        }

		$imageWidth = 100;
		if (isset($filters['image_width']) && !empty($filters['image_width'])) {
			$imageWidth = $filters['image_width'];
		}

		$imageHeight = 100;
		if (isset($filters['image_height']) && !empty($filters['image_height'])) {
			$imageHeight = $filters['image_height'];
		}

		$catalogImageHelper = Mage::helper('catalog/image');

        $result = array();

        foreach ($galleryData['images'] as &$image) {
            // $result[] = $this->_imageToArray($image, $product);

            $arr = $this->_imageToArray($image, $product);
			
			// // Get & set the url for the resized image
			$arr['url'] = (string)$catalogImageHelper->init($product, 'image')->resize($imageWidth, $imageHeight);
			// error_log('itemsWithResized::url=['.$arr['url'].']');
			
			$result[] = $arr;
        }

        return $result;
    }

}
